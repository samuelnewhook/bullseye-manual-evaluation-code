#!/usr/bin/python3
import csv
import numpy as np
from utils import Circle, LineSegment, rotate_x_ax, rotate_z_ax
import argparse

def wire_position(center_of_circ, trigonum, inf_process, wire_intersection, wire_top):
    """ The offset from estimated center of glenoid articular surface and elevation and azimuth angles
        wrt the local coordinate system origin
        The coordinate system is generated from the center of the circle, the trigonum and the inferior axis.
        Then the intersection and top of wire are transformed into that coordinate system.
        The axes are returned as well.
        Points are input in RAS coordinates.
    """
    z_vec = LineSegment(np.vstack((trigonum, center_of_circ))) # +Lateral
    z_axis = z_vec.slope
    second_vec = LineSegment(np.vstack((inf_process, center_of_circ)))
    second_vec_unit = second_vec.slope
    x_axis = LineSegment(np.cross(second_vec_unit, z_axis), center_of_circ).slope # +Anterior
    y_axis = LineSegment(np.cross(z_axis, x_axis), center_of_circ).slope # +Superior
    rotation_mat = np.vstack((x_axis, y_axis, z_axis))
    translation_mat = - center_of_circ
    total_mat = np.vstack((np.hstack((rotation_mat, np.expand_dims(translation_mat.T, axis=1))), np.array([0, 0, 0, 1])))
    if wire_intersection is None and wire_top is None:
        return None, None, x_axis, y_axis, z_axis, rotation_mat, translation_mat
    intersection = np.matmul(rotation_mat, (wire_intersection - center_of_circ))
    top = np.matmul(rotation_mat, (wire_top - center_of_circ).T)
    return intersection, top, x_axis, y_axis, z_axis, rotation_mat, translation_mat

def ax_plot_points(x_ax, y_ax, z_ax, circ_center):
    ls_x = LineSegment(x_ax, circ_center)
    ls_y = LineSegment(y_ax, circ_center)
    ls_z = LineSegment(z_ax, circ_center)
    return np.vstack((
        ls_x.get_point_on_line(10.),
        ls_y.get_point_on_line(10.),
        ls_z.get_point_on_line(10.)
        ))

def row_to_dat(row, is_preop):
    i = 0
    if is_preop:
        i = -6
    side_of_body = row[23 + i]
    wire_top = None
    wire_intersection = None
    if not is_preop:
        wire_top = np.array([float(f) for f in row[2:5]])
        wire_intersection = np.array([float(f) for f in row[5:8]])
    circ_pts = np.reshape(np.array([float(f) for f in row[8+i:17+i]]), (3, 3))
    trigonum = np.array([float(f) for f in row[17+i:20+i]])
    inf_process = np.array([float(f) for f in row[20+i:23+i]])
    return side_of_body, wire_top, wire_intersection, circ_pts, trigonum, inf_process


def pos_scan_from_file(pos_file, is_preop):
    with open(pos_file) as results:
        reader = csv.reader(results, delimiter=',')
        has_header_been_read = False
        write_data = []
        if not is_preop:
            write_data.append(', '.join(('name', 'description', 'offset anterior mm', 'offset superior mm', 'inclination superior deg', 'version anterior deg')))
        else:
            write_data.append(', '.join(('name', 'description')))
        for row in reader:
            if not has_header_been_read:
                print('\n'.join(row[0:2]))
                has_header_been_read = True
                continue
            print('---------------------------------')
            print('\n'.join(row[0:2]))
            side_of_body, wire_top, wire_intersection, circ_pts, trigonum, inf_process = row_to_dat(row, is_preop)
            circ = Circle(circ_pts)
            center_of_circ = circ.circle_center
            print(f'center of circle: {center_of_circ}')
            intersection, top, x_axis, y_axis, z_axis, rotation_mat, translation_mat = wire_position(center_of_circ, trigonum, inf_process, wire_intersection, wire_top)
            rot_mat = rotate_z_ax(rotate_x_ax(rotation_mat, np.pi / 2), np.pi / 2)
            if side_of_body == 'L':
                rot_mat = rotate_z_ax(rot_mat, -np.pi)
            rot_mat_ex = np.eye(4)
            rot_mat_ex[:3, :3] = rot_mat
            t_mat = np.eye(4)
            t_mat[[0, 1, 2], 3] = -center_of_circ.T
            total_mat = np.matmul(rot_mat_ex, t_mat)
            print(f'total mat {total_mat}')
            if intersection is not None and top is not None:
                cross_vec_z_plane = np.array([0., 1.])
                wire_segment = LineSegment(np.vstack((intersection, top)))
                wire_vec = wire_segment.slope
                version_angle = np.arccos(np.dot(cross_vec_z_plane, wire_vec[[0, 2]]) / (np.linalg.norm(wire_vec[[0, 2]]) * np.linalg.norm(cross_vec_z_plane))) * 180 / (2 * np.pi)
                inclination_angle = np.arccos(np.dot(cross_vec_z_plane, wire_vec[[1, 2]]) / (np.linalg.norm(wire_vec[[1, 2]]) * np.linalg.norm(cross_vec_z_plane))) * 180 / (2 * np.pi)
                # Anterior is positive for version, if the wire vector is less than zero, we have negative version
                if wire_vec[0] < 0:
                    version_angle = -version_angle
                # Similarly for inclincation, if the wire vector is less than zero, we have negative inclination
                if wire_vec[1] < 0:
                    inclination_angle = -inclination_angle
                print(f'the wire is placed at angles (deg): inclination: {inclination_angle}, version_angle: {version_angle} wrt to the anatomical coordinate system')
                print(f'wire intersection is offset {intersection[0]} mm anterior and {intersection[1]} superior in the face of the glenoid')
                write_data.append(', '.join((row[0], row[1], str(intersection[0]), str(intersection[1]), str(inclination_angle), str(version_angle))))
    print('\n'.join(write_data))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Processing Markups to get wire position and orient CT scans of the scapula.')
    parser.add_argument('data_file', metavar='d', type=str, help='Specifies relative filepath to the scapula/wire position data file.')
    parser.add_argument('preop', metavar='p', type=int, default=0, help='Whether or not we are dealing with a scan with a wire, or a preop scan.')
    args = parser.parse_args()
    pos_scan_from_file(args.data_file, args.preop)


