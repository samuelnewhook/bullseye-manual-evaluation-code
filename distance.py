#!/usr/bin/python3
from utils import LineSegment
import numpy as np

if __name__ == "__main__":
    p1 = [0.939, -1.781, 9.255]
    p2 = [-0.460, 1.468, 9.685]
    ls = LineSegment(np.array([p1, p2]))
    magnitude = ls.segment_length
    print(f'The magnitude of the vector running from p1 to p2 is {magnitude}')
    print(f'Half the magnitude of the vector running from p1 to p2 is {magnitude/2}')
