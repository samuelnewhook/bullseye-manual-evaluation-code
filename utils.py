#!/usr/bin/python3
import numpy
from numpy import linalg as LA
from sympy.solvers.solveset import nonlinsolve
from sympy.core.symbol import symbols
import pprint
pp = pprint.PrettyPrinter()
EPSILON = 1.0E-5

def rotate_x_ax(mat, deg):
    """ Rotate a column vector. Use radians
    """
    rotate_mat = numpy.array([[1, 0, 0], [0, numpy.cos(deg), -numpy.sin(deg)], [0, numpy.sin(deg), numpy.cos(deg)]])
    return numpy.matmul(rotate_mat , mat)

def rotate_z_ax(mat, deg):
    """ Rotate a column vector. Use radians
    """
    rotate_mat = numpy.array([[numpy.cos(deg), - numpy.sin(deg), 0], [numpy.sin(deg), numpy.cos(deg), 0], [0, 0, 1]])
    return numpy.matmul(rotate_mat , mat)


class Circle:
    """ Circle in 3D from three points
        First, find the system of unit vectors defining the axis constrained
        by the three input points.
        Next, apply the translation and rotation matrices to place the first point
        at the origin and the other points on a single plane.
        Solve the system of equations for a circle and calculate the center and radius.
        Invert the translation and rotation to obtain the center of the circle in the original
        coordinate space.
    """
    def __init__(self, pts):
        n1 = LineSegment(pts[0:2])  # x-axis
        n2 = LineSegment(pts[[0, 2], :])
        z_axis = LineSegment(numpy.cross(n1.slope, n2.slope), pts[0]).slope
        y_axis = LineSegment(numpy.cross(z_axis, n1.slope), pts[0]).slope
        translation = numpy.vstack((pts[0], pts[0], pts[0]))
        pts_new_origin = pts - translation
        rotation = numpy.vstack((n1.slope, y_axis, z_axis))
        rotated = numpy.matmul(rotation, pts_new_origin.T)
        self._pts_2D = rotated[[0, 1], :].T
        x_off, y_off, rad = self._x_off(), self._y_off(), self._r()
        self._center_2D = numpy.array([x_off, y_off])
        self._radius_2D = rad
        self._inverse_tform = numpy.linalg.inv(rotation)
        self._circle_center = numpy.matmul(self._inverse_tform, numpy.array([x_off, y_off, 0.]).T).T + pts[0]

    def _x_off(self):
        p = self._pts_2D
        return (p[0][0]**2*p[1][1] - p[0][0]**2*p[2][1] + p[0][1]**2*p[1][1] - p[0][1]**2*p[2][1] - p[0][1]*p[1][0]**2 - p[0][1]*p[1][1]**2 + p[0][1]*p[2][0]**2 + p[0][1]*p[2][1]**2 + p[1][0]**2*p[2][1] + p[1][1]**2*p[2][1] - p[1][1]*p[2][0]**2 - p[1][1]*p[2][1]**2) / (2*(p[0][0]*p[1][1] - p[0][0]*p[2][1] - p[0][1]*p[1][0] + p[0][1]*p[2][0] + p[1][0]*p[2][1] - p[1][1]*p[2][0]))

    def _y_off(self):
        p = self._pts_2D
        return -(p[0][0]**2*p[1][0] - p[0][0]**2*p[2][0] - p[0][0]*p[1][0]**2 - p[0][0]*p[1][1]**2 + p[0][0]*p[2][0]**2 + p[0][0]*p[2][1]**2 + p[0][1]**2*p[1][0] - p[0][1]**2*p[2][0] + p[1][0]**2*p[2][0] - p[1][0]*p[2][0]**2 - p[1][0]*p[2][1]**2 + p[1][1]**2*p[2][0])/(2*(p[0][0]*p[1][1] - p[0][0]*p[2][1] - p[0][1]*p[1][0] + p[0][1]*p[2][0] + p[1][0]*p[2][1] - p[1][1]*p[2][0]))

    def _r(self):
        p = self._pts_2D
        return numpy.abs(-numpy.sqrt((p[0][0]**2 - 2*p[0][0]*p[1][0] + p[0][1]**2 - 2*p[0][1]*p[1][1] + p[1][0]**2 + p[1][1]**2)*(p[0][0]**2 - 2*p[0][0]*p[2][0] + p[0][1]**2 - 2*p[0][1]*p[2][1] + p[2][0]**2 + p[2][1]**2)*(p[1][0]**2 - 2*p[1][0]*p[2][0] + p[1][1]**2 - 2*p[1][1]*p[2][1] + p[2][0]**2 + p[2][1]**2))/(2*(p[0][0]*p[1][1] - p[0][0]*p[2][1] - p[0][1]*p[1][0] + p[0][1]*p[2][0] + p[1][0]*p[2][1] - p[1][1]*p[2][0])))

    @property
    def pts_2D(self):
        return self._pts_2D

    @property
    def circle_center(self):
        return self._circle_center

    @property
    def inverse_tform(self):
        return self._inverse_tform

    @property
    def x_off(self):
        return self._center_2D[0]

    @property
    def y_off(self):
        return self._center_2D[1]

    @property
    def r(self):
        return self._radius_2D


class Plane:
    ''' Plane of form ax + by + cz = d
        Load any three points as pts [p0; p1; p2] where pn is [x, y, z]
    '''
    def __init__(self, pts):
        self._a, self._b, self._c, self._d = self._plane_from_points(pts)
        abs_abc = numpy.absolute([self._a, self._b, self._c])
        self._max_ind_norm = numpy.where(abs_abc == numpy.max(abs_abc))[0]

    def _plane_from_points(self, pts: numpy.array):
        v1 = pts[2, :] - pts[0, :]
        v2 = pts[1, :] - pts[0, :]
        cp = numpy.cross(v1, v2)
        a, b, c = cp
        d = numpy.dot(cp, pts[2, :])
        return a, b, c, d

    def intersection_pt(self, line):
        plane_abc = numpy.array([self._a, self._b, self._c])
        denom = numpy.dot(plane_abc, line.slope)
        if denom == 0:
            return numpy.array([numpy.nan, numpy.nan, numpy.nan]), numpy.nan
        s = (self._d - numpy.dot(plane_abc, line.initial_pt)) / denom
        return line.get_point_on_line(s), s

    @property
    def largest_index_in_norm(self):
        return self._max_ind_norm

    @property
    def a(self):
        return self._a

    @property
    def b(self):
        return self._b

    @property
    def c(self):
        return self._c

    @property
    def d(self):
        return self._d

    @property
    def is_vertical(self):
        return self._b == 0

    @property
    def normal(self):
        return numpy.array([self._a, self._b, self._c])


class LineSegment:
    """
        Generates a line segment object given two points
        Load any two points as pts [p0; p1] where pn is [x, y, z]
        The line segment spans p0 --> p1
        slope is normalized
        Essential represents the line in a form y = mx + b but for
        3D coordinate system
    """

    def __init__(self, *args):
        # if length of arguments is 1, we expect a matrix containing the two
        # input points vertically stacked
        if len(args) == 1:
            self._slope, self._initial_pt, self._final_pt = \
                    self._line_from_pts(args[0])
            self._segment_length = LA.norm(self._final_pt - self._initial_pt)
        # if length is 2, we expect a vector and a starting point
        elif len(args) == 2:
            self._slope = args[0] / LA.norm(args[0])
            self._initial_pt = args[1]
            self._final_pt = self.get_point_on_line(1.0)
            self._segment_length = LA.norm(self._final_pt - self._initial_pt)

    def __str__(self):
        return f"Line segment spanning from {self.initial_pt} to {self.final_pt}"

    def _line_from_pts(self, pts):
        xs = pts[1, 0] - pts[0, 0]
        ys = pts[1, 1] - pts[0, 1]
        zs = pts[1, 2] - pts[0, 2]
        slope = numpy.array([xs, ys, zs])
        norm_slope = slope / LA.norm(slope)
        xi = pts[0, 0]
        yi = pts[0, 1]
        zi = pts[0, 2]
        return norm_slope, numpy.array([xi, yi, zi]), numpy.array([pts[1, 0], pts[1, 1], pts[1, 2]])

    def get_point_on_line(self, s):
        return self._slope * s + self._initial_pt

    def vec_along_slope(self, s):
        return self._slope * s

    def intersection_of_line(self, line):
        m2 = line.slope
        m1 = self.slope
        yi2 = line.initial_pt[1]
        yi1 = self.initial_pt[1]
        xi2 = line.initial_pt[0]
        xi1 = self.initial_pt[0]
        denom = m1[1] * m2[0] - m1[0] * m2[1]
        if LA.norm(denom) < EPSILON:
            return False
        s2 = (m1[0] * (yi2 - yi1) - m1[1] * xi2 + m1[1] * xi1) / denom
        s1 = (m2[0] * (yi2 - yi1) + m2[1] * xi1 - xi2 * m2[1]) / denom
        possible_pt_1 = line.get_point_on_line(s2)
        possible_pt_2 = self.get_point_on_line(s1)
        if LA.norm(possible_pt_1[2] - possible_pt_2[2]) > EPSILON:
            return False
        return possible_pt_1

    def get_perpendicular(self, pt, axis=2):
        axis_to_swap = [axis - 2, axis - 1]
        negative_reciprocal_slope = self.slope.copy()
        tmp = negative_reciprocal_slope[axis_to_swap[0]]
        negative_reciprocal_slope[axis_to_swap[0]] = negative_reciprocal_slope[axis_to_swap[1]]
        negative_reciprocal_slope[axis_to_swap[1]] = -tmp
        return LineSegment(negative_reciprocal_slope, pt)

    @property
    def slope(self):
        return self._slope

    @property
    def initial_pt(self):
        return self._initial_pt

    @property
    def final_pt(self):
        return self._final_pt

    @property
    def segment_length(self):
        return self._segment_length

