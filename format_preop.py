#!/usr/bin/python3
from format_markups import format_markups_output
import os


if __name__ == "__main__":
    format_markups_output(os.path.join('data', 'markups', 'preop/'), 'data/position_data_files/preop_position_data.csv')
