# Notes for manual computation of Bullseye accuracy

## Manual Evaluation Steps (previous)
https://docs.google.com/document/d/1fmp6SO_ctudMrfTENwGgWZo7vBzipePGDEBkZ_83iyA/edit?usp=sharing

## Software Used
1. Slicer 4.10.2
2. Meshlab 

## Preparation of optical segmentations
Using colour and shape to segment the glenoid surface by removing vertex in the same manner as the bullseye manual evaluation (previous). 

## Step-by-step instructions
1. Load the preop-ct scan using the DICOM module
2. Segment the Bone data using the segment editor module in Slicer. Currently using the lower value 200
3. When Segmenting the postop CT, using value [1293.35, 2970.9] which includes bone and excludes the wire
4. Using Islands, select keep largest island and ensure that the glenoid is maintained
5. Trim with scissors if necessary, repeat the *keep largest island step* to ensure that only the scapula is segmented
6. In the segmentation module, export this segmentation to a new labelmap  
7. Go to the model maker and create a new model from the labelmap
8. Import optical tracker model and wire model
9. Import segmented optical scan of tracker and glenoid surface 

## Registering Pre-op CT to Post-op CT
1. Create a segmentation for the post-op CT
2. Place fiducials on easy to find landmarks in the Pre and Post op CTs
3. Run fiducial registration
4. Now using Mutual Information instead of MSE, run the General Registration 


## Registering the Tracker
1. Use fiducial registration and markups to prealign the optical scan (moving) and model of the fiducial (fixed).
2. Surface Registration next and apply to glenoid optical scan as well.

## Registering the optical scan of the glenoid and the CT model
1. Prealign the CTs and segmentations using the wire_position.py script
2. Rotate the wire and the fiducial by 90 degrees to match the anatomical orientation or the scan
3. Refine the prealignment with a rotation about the axis of the wire model
4. Perform surface registration on the preop-ct scapula model and the optical surface image with the surface scan as the moving image
5. Invert the transform and apply to the post-op scapula model
6. Place the markers for error measurement and use the bullseye_measurement_error.py script to calculate the error of the measured wire position and the true wire_position 


# MRI Error Computation Manual Steps
1. Load in the Pre-op CT, Post-op CT and MRI scans
2. Rename the scans in the *data* module to *PREOPCT*, *POSTOP1* and *PREOPMRI* respectively
3. In the *Segment Editor* module, create a segment and name it *PREOPSEG*
4. Under *Master Volume* select *PREOPCT*
5. Under *Effects* select *Threshold* and change the *Threshold Range* to include scapula bone (150, 2889)
6. Hit *Apply* and select *Show 3D*
7. Under *Effects* select *Islands*, choose *Keep largest island* and hit *Apply*
8. Using *Scissors* and *Islands*, isolate the glenoid bone
9. Optional: Apply the *Smoothing* *Effect* to the segmentation to close over some holes
10. Under *Margin* select *Grow* with *Margin Size* to a sufficient 1mm
11. Segment the cartilege from the MRI with the value (12511, 12513)
12. Save the segmentation to STL file
13. In *MeshLab* segment the lateral surface of the segmentation to make a one side segmentation
14. Import the one sided segmentation. Use this segment to register the optical scan and proceed as above

# Automation of alignment and display intraoperatively
## Critical steps for automation
- Segmentation of the Optical Scan
- Import (Easy)
- Pre-alignment of tracker model and tracker scan
- Pre-alignment of scapula model and glenoid scan
- Anatomic alignment for display

### Segmentation of the Optical Scan
- The optical scan is segmented manually by colour, and position for the tracker
- The glenoid is more complicated, uses colour and shape for segmentation
	- Possibly, the glenoid could be grown using the tracker as a starting location
### Import
- Load manually (drag and drop) to start, possibly include a service to monitor a folder for quick import and verification
### Pre-alignment
- PCA for the tracker alignment
- If the segmentation of the glenoid is easy to complete, we may be able to use a statistical shape model to identify the glenoid fossa and use PCA again for pre-alignment
### Display
- SSM for this as well, and use of the scapular plane alignment method could work well as well.
