# bullseye-manual-evaluation-code

## Bullseye Measurement Error
Edit the results.csv file to include the RAS coordinates of the three placed wire fiducials.
Run ```./bullseye_measurement_error.py```

## Model Growing
1. Measure the distance from the bone surface on the PREOP CT scan to the bone surface on the humeral head using the markups module.

## Wire Position Calculation
To find the position of the wire, we will calculate the inclination, version and offset from glenoid articular surfaced. The method returns inclination (superior, inferior), version (anterior, posterior) and S-I and A-P offset from centre.

### Scapular plane definition
We define the scapular plane by 
1. Fitting a circle to the inferior two thirds of the glenoid.
2. Placing fiducials on the inferior angle and trigonum.
3. Define the vector from the trigonum to the circle centre as the medial-lateral axis (lateral +)
4. Use the cross product of the vector from the inferior angle to the circle centre and the medial-lateral axis to form the anterior-posterior axis (anterior +).
	- The cross product will give us the anterior vector on the right scapula. In order to have anterior + for the left scapula, we take the negative
5. Finally, the cross product of the A-P and M-L vectors will give us the superior-inferior axis (inferior +)

### Wire Placement
To define the wire placement, the A-P offset and the S-I offset are calculated by translating the wire intersection and wire top points so that the glenoid center is at the origin (wi - gc). Then, we apply the rotation matrix that aligns the coordinate systems (placing the wire coordinates into the newly defined scapular plane). We then find the vector from the wire intersection to the wire top and calculate the angle made between the wire in the 2D plane made by the I-S and M-L vectors (inclination) and the 2D plane made by the A-P and M-L vectors (version). If the point defining the top of the wire is in the +'ve in the I-S plane, the inclination angle is +'ve and if the wire top is +'ve in the A-P plane then the verison angle is positive.
