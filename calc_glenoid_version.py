import numpy as np
from utils import LineSegment


def main(fr_p_1, fr_p_2, intermediate_joint_p_1, intermediate_joint_p_2):

    # Friedman Line:
    fr_line = LineSegment(np.array([fr_p_1, fr_p_2]))

    # Intermediate Joint Line:
    intermediate_joint_line = LineSegment(np.array([intermediate_joint_p_1, intermediate_joint_p_2]))

    # Get Intersection Point:
    intersection_point = fr_line.intersection_of_line(intermediate_joint_line)

    # Create line to intersection point:
    intersection_line = LineSegment(np.array([fr_p_1, intersection_point]))

    # Get perpendicular line:
    perpendicular_line = intersection_line.get_perpendicular(intersection_point)

    # Get angle between lines, slopes are already normalized:
    angle_between_lines = np.arccos(np.dot(perpendicular_line.slope,
                                           intermediate_joint_line.slope))
    print('Calculated Angle using Line Segments: ', (angle_between_lines * 180 / np.pi)-180)


def simpler(fr_p_1, fr_p_2, intermediate_joint_p_1, intermediate_joint_p_2):
    # make the Friedman line with two selected points
    # fr_p_1 = np.array([-115.339, -0.102, 4.954])
    # fr_p_2 = np.array([-2.272, 0.788, 4.954])

    fr_vec = fr_p_2 - fr_p_1
    fr_vec_normalized = fr_vec / np.linalg.norm(fr_vec)

    # Intermediate Joint Line:
    # intermediate_joint_p_1 = np.array([-2.977, -13.410, 4.954])
    # intermediate_joint_p_2 = np.array([2.125, 13.117, 4.954])
    intermediate_joint_vec = intermediate_joint_p_2 - intermediate_joint_p_1
    intermediate_joint_vec_normalized = intermediate_joint_vec / np.linalg.norm(intermediate_joint_vec)

    # Make Perpendicular vector
    perpendicular_fr_vec = fr_vec_normalized.copy()
    tmp = perpendicular_fr_vec[0]
    perpendicular_fr_vec[0] = perpendicular_fr_vec[1]
    perpendicular_fr_vec[1] = -tmp

    angle_between_lines = np.arccos(np.dot(perpendicular_fr_vec,
                                           intermediate_joint_vec_normalized))
    print('Calculated Angle Simpler Method: ', angle_between_lines * 180 / np.pi - 180)


if __name__ == "__main__":
    names = ['GL1706594R', 'GL1706594L', 'GL1706750R', 'GL1707259L', 'UK2L', 'GL1707121R']
    fr_p_1_list = [np.array([-115.339, -0.102, 4.954]), np.array([0.273, -115.574, 0.040]), np.array([1.982, -104.679, -0.205]), np.array([1.152, -106.523, -0.035]), np.array([6.391, -117.567, -0.025]), np.array([0.345, -106.151, -0.035])]
    fr_p_2_list = [np.array([-2.272, 0.788, 4.954]), np.array([0.101, -3.836, 0.040]), np.array([0.065, -2.257, -0.205]), np.array([0.498, -1.636, -0.035]), np.array([-0.079, -1.880, -0.025]), np.array([-0.358, -1.800, -0.035])]
    intermediate_joint_p_1_list = [np.array([-2.977, -13.410, 4.954]), np.array([13.378, 1.682, 0.040]), np.array([10.438, -2.015, -0.205]), np.array([12.003, 0.921, -0.035]), np.array([14.489, -1.434, -0.025]), np.array([15.232, -3.020, -0.035])]
    intermediate_joint_p_2_list = [np.array([2.125, 13.117, 4.954]), np.array([-12.659, -4.699, 0.040]), np.array([-11.233, -1.741, -0.205]), np.array([-11.555, -1.819, -0.035]), np.array([-15.251, 2.704, -0.025]), np.array([-14.186, 3.488, -0.035])]
    for _, (name, fr_p_1, fr_p_2, intermediate_joint_p_1, intermediate_joint_p_2) in enumerate(zip(names, fr_p_1_list, fr_p_2_list, intermediate_joint_p_1_list, intermediate_joint_p_2_list)):
        print(name)
        main(fr_p_1, fr_p_2, intermediate_joint_p_1, intermediate_joint_p_2)
        simpler(fr_p_1[:-1], fr_p_2[:-1], intermediate_joint_p_1[:-1], intermediate_joint_p_2[:-1])
