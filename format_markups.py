#!/usr/bin/python3
import csv, glob, os, argparse

def format_markups_output(dirname, data_file):
    files = glob.glob(os.path.join(dirname, '*.csv'))
    procedure_list = {}
    print(f'files {files}')
    for f in files:
        MARKUP_LIST = {'circ1': [], 'circ2': [], 'circ3': [], 'inf_angle': [], 'trigonum': [], 'wire_top': [], 'wire_intersection': []}
        with open(f) as csv_file:
            reader = csv.reader(csv_file, delimiter='\t')
            for row in reader:
                name = row[11]
                if name not in MARKUP_LIST.keys():
                    print(f'FILE {f} IS NOT PORPERLY FORMATTED, name {name} is not accepted')
                    continue
                MARKUP_LIST[name] = [row[1], row[2], row[3]]
        procedure_list[os.path.basename(f).split('.')[0]] = MARKUP_LIST
    print(procedure_list)
    rewrite_data = []
    with open(data_file, 'r') as dat:
        reader = csv.reader(dat, delimiter=',')
        for row in reader:
            name = row[0]
            # Edit if the file is in procedure list
            if name in procedure_list.keys():
                prod_dat = procedure_list[name]
                side_of_body = ['L' if 'L' in name else 'R']
                formatted_data = ','.join(row[:2] + prod_dat['wire_top'] + prod_dat['wire_intersection'] + prod_dat['circ1'] + prod_dat['circ2'] + prod_dat['circ3'] + prod_dat['trigonum'] + prod_dat['inf_angle'] + side_of_body)
                rewrite_data.append(formatted_data)
            else:
                rewrite_data.append(','.join(row))
    with open(data_file, 'w') as dat:
        print(f'rewrite_data, {rewrite_data}')
        dat.write('\n'.join(rewrite_data))




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Script for formatting csv files with markup positions. Requires a position file to write to. First, append a line to the position file with the name of the patient, side [R, L] and the operation number. An example is 121ROP1. Also on this line, separated by a comma, include a description. Then, in a folder containing all of the markup files, create a file with the same format for the name i.e. 121ROP1.csv. In this file, paste in the markups position values. Running this script will parse the markup file and write the output to the position file. An example of running the file would be $ python /path/to/folder/containing/markup/files/ /path/to/positions/file.csv. We are then ready to run the wire_position script.')
    parser.add_argument('markup_folder', metavar='m', type=str, help='Specifies relative path the the directory with the markup files.')
    parser.add_argument('data_file', metavar='d', type=str, help='File to write the positions to.')
    args = parser.parse_args()
    if os.path.isdir(args.markup_folder) is False:
        raise NotADirectoryError(f'Please provide a valid markups directory, {args.markup_folder} does not exist!')
    if os.path.isfile(args.data_file) is False:
        raise FileNotFoundError(f'Please provide a valid output file, {args.data_file} does not exist!')
    format_markups_output(args.markup_folder, args.data_file)
