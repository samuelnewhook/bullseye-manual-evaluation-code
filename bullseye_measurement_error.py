#!/usr/bin/python3

import numpy as np
import csv


def calc_offset_error(f1: np.array, f2: np.array, dy: float) -> float:
    """
    Calculates the offset error between the predicted wire
    insertion point and the real wire insertion point
    f1: The fiducial placed on the top of the wire segment
    f2: The fiducial placed on the bottom of the wire segment
    dy: The AP coordinate at the glenoid surface where the wire enters
    """
    v = f2-f1
    t = (dy - f1[1])/v[1]
    start = f1 + v*t
    error = np.sqrt(start[0]*start[0] + start[2]*start[2])
    return error


def calc_angle_error(f1: np.array, f2: np.array, dy: float) -> float:
    """
    Calculates the error between the actual insertion angle and the predicated
    insertion angle
    f1: The fiducial placed on the top of the wire segment
    f2: The fiducial placed on the bottom of the wire segment
    dy: The AP coordinate at the glenoid surface where the wire enters
    """
    # RAS coords
    # this assumes for now that the predicted wire is oriented A-P
    v = f2-f1
    w = np.array([0., 1., 0.])
    alpha_error = np.arccos(w.dot(v) / np.linalg.norm(v)) * 180. / np.pi
    return alpha_error


if __name__ == "__main__":
    with open('data/position_data_files/results.csv') as results:
        reader = csv.reader(results, delimiter=',')
        has_header_been_read = False
        for row in reader:
            if not has_header_been_read:
                print('\n'.join(row[0:2]))
                has_header_been_read = True
                continue
            print('-------------------------------')
            print('\n'.join(row[0:2]))
            p2 = np.array([float(f) for f in row[2:5]])
            p1 = np.array([float(f) for f in row[5:8]])
            dy = float(row[8])
            offset = calc_offset_error(p1, p2, dy)
            angle_error = calc_angle_error(p1, p2, dy)
            print(f'Angle Error is {angle_error}')
            print(f'Offset Error is {offset}')
